$(function(){
	var navbar = $('.navbar');
	
	$(window).scroll(function(){
		if($(window).scrollTop() <= 40){
      navbar.removeClass('navbar-scroll');
      $("#imgX").attr("src","./images/468054_beach-logo.svg");
		} else {
      navbar.addClass('navbar-scroll');
      $("#imgX").attr("src","./images/468054_beach-logo2.svg");
		}
  });


});

$(function () {

  $('.menu').click(function () {

    $('.hidden').show();
    $('.show').hide();

  });

  $('.hideButton').click(function () {

    $('.hidden').hide();
    $('.show').show();

  });

});


/*

$(document).on('ready', function() {
  
  $(".center").slick({
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 5,
    slidesToScroll: 3
  });

});
*/
$(document).ready(function(){
  
$('.autoplay').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  adaptiveHeight: true,
});

$('.center').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
});